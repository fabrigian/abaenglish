<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Iteration 2</title>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.js"></script> 
   
    <script src="js/jquery.validate.min.js"></script> 
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

      
    <script>
      
   <?php
   
   $partner=$_GET["partner"];
   $campaign=$_GET["campaign"];
   
   ?>
      
      function ridireziona() {
	   var delay = 4000; 
				setTimeout(function(){ window.location = "index3.php"; }, delay);
      }
 
  $(document).ready(function() {
  
    $("#myForm").validate({
              ignore: [],
		rules: {
			  	name: "required",
				surname: "required",
				email: {
					required: true,
					email: true
				},
				
			    },
		messages: {
			  
				nome: "Insert your name",
				cognome: "Insert your surname",
				email: "Insert a valid email",
				}
		});
  
  $("#sender").click(function(){
    
    $("#message").empty();
    
    // capture the click
        if($("#myForm").valid()){   // test for validity
            // do stuff if form is valid

	    if ($('#newsletter').is(':checked')) {
	      var news=1;
	    } else {
	      var news=0;
	    }
	    
		  $.ajax({
		    type: "POST",
		    url: "form.php",
		    data: { name: $( "#name" ).val(),
		    surname: $( "#surname" ).val(),
		    phone: $( "#phone" ).val(),
		    email: $( "#email" ).val(),
		    campaign: <?php echo $campaign; ?>,
		    partner: <?php echo $partner; ?>,
		    newsletter: news
		    },
		    success: function(result){
		  
		    console.log(result);
			
		      if (result[0] == 1) {
			      
			     $("#message").html("<div class='ok'> Ok. You're a new user ready to learn in English with ABA ENGLISH <br/> You will be redirected to the Statistic page </div>");

			       ridireziona();
			      
			      
                            } else {
			      
			       $("#message").html("<div class='no'>The Email you provided is already associated to an existent user</div>");
			    }
		    },
		    dataType: "json"
		  });
	
	return false;
	    
        } else {
            
	    
	    
        }
	
  });
	
	 });
   
      
    </script>
    <div class="container">
    <?php require("header.php"); ?>
  
    	<div class="row" id="riga-form">
	  
	  <div id="formcol" class="col-md-12 col-sm-12 col-xs-12">
	    
	    <form id="myForm" action="form.php" method="post" accept-charset="utf-8" novalidate="true">
	      
	      <div class="titolo_form"> Register  </div>
	      
	      <div class="row">
		<div class="col-sm-4 etichetta"> Name <span class="ast">*</span></div>
		<div class="col-sm-4 campo"> <input type="text" id="name" name="name" placeholder="Name"> </div>
	
	      </div>
	      
	          <div class="row">
		<div class="col-sm-4 etichetta"> Surname <span class="ast">*</span></div>
		
		<div class="col-sm-4 campo"> <input type="text" id="surname" name="surname" placeholder="Surname"> </div>
	      </div>
	       
		 
		 <div class="row">
		  <div class="col-sm-4 etichetta"> Email <span class="ast">*</span></div>
		  
		  <div class="col-sm-8 campo"> <input type="text" id="email" name="email" id="email" placeholder="ex:mymail@example.com" size="45"> </div>
		 </div>
		 
		 
		 <div class="row">
		  <div class="col-sm-4 etichetta"> Phone <span class="ast"></span></div>
		  
		  <div class="col-sm-8 campo"> <input type="text" name="phone" id="phone" placeholder="ex:3288739553" size="45"> </div>
		 </div>
		
		
		
		  </div>
		  </div>
		
		
		  
		  <div class="row">
		    
		    <div class="col-sm-4 etichetta"> Newsletter </div>
		    <div class="col-sm-8 campo"> <input type="checkbox" id="newsletter" name="newsletter"> </div>
		    
		  </div>
		  
		  
	    
		 
		 <div id="invio" class="row">
		 <div class="col-sm-12">
		  <button class="btn btn-primary" id="sender"> Send </button>
		  
		
		 </div>
		 </div>
		  

	    </form>
	      <div class="row">
	         <div class="col-sm-12">
		  <div id="message"> </div>
		 </div>
	</div>
	      
	       <div class="row">
		<div class="col-sm-4">
		  <a href="index.php">Iteration 1</a>
		</div>
		<div class="col-sm-4">
		
		</div>
		
		
		<div class="col-sm-4">
		  <a href="index3.php">Iteration 3</a>
		</div>
		
	      </div>
	      
	  </div>
	
	


  </body>
</html>
